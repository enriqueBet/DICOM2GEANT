'''
Creates an input on Data.dat using the name of dicom images
by: Enrique Betancourt
e-mail fis.ebetancourt@gmail.com
'''

import sys,os,argparse
from subprocess import call

def CreateInput(path,name):
    '''
    Arguments:
        path - Path were the dicom images are stored.
    '''
    # Create a list of all dicom images in the directory
    dicoms = []
    for dicom in os.listdir(path):
        if ".dcm" in dicom:
            dicoms.append(dicom)

    # Sort the list
    dicoms.sort()

    ## Writes the data file
    # Comprension value
    comp = 8

    # Number of dicom images
    num = len(dicoms)

    # Writes the list of Dicoms
    with open("Materials.dat","r") as materials:
        with open(name,"w") as writer:
            writer.write("%s\n"%comp)
            writer.write("%s\n"%num)
            for i in range(len(dicoms)):
                file_name = dicoms[i].replace(".dcm","\n")
                writer.write(file_name)
            # Materials
            lines = materials.readlines()
            num_mat = len(lines)
            writer.write("%s\n"%num_mat)
            for line in lines:
                print(line)
                writer.write(line)
    print("[info] The dicom data file was successfully created: %s"%(name))

def CopyDicoms(path):
    '''
    In case that you are using a different directory than shared/ copy a symbolic link of all dicom images on the path directory to the build directory
    '''
    root = "/".join(os.getcwd().split("/")[:-1])
    build = os.getcwd()
    path = os.path.abspath(path)
    for dicom in os.listdir(path):
        if ".dcm" in dicom:
            # Creates symbolic links
            order = "cp %s ."%os.path.join(build,dicom)
            call(order,shell=True)
    print("[info] All required symlinks were created successfully")

def main():
    # Argument parser
    ap = argparse.ArgumentParser()
    ap.add_argument("-n","--name",help="Name of the output file, by defaultL: Data.dat")
    ap.add_argument("-p","--path",help="Path to the dicom images, by default: shared/")
    args = ap.parse_args()

    if args.name:
        name = args.name
    else:
        name = "Data.dat"

    if args.path:
        path = args.path
    else:
        root = "/".join(os.getcwd().split("/")[:-1])
        path = os.path.join(root,"shared")

    # Create the dicom data input file
    CreateInput(path,name)

    # Copy all the links from the dicom files to the build folder
    CopyDicoms(path)

if __name__ == "__main__":
    main()




