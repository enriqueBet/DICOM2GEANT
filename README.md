## Dicom Input Creator

# Overview

This code was made as a complement of the DICOM example made in GEANT4. The objective is to develop an easy way to include sets of DICOM images into the required input.

# Usage

In order to make this code work, you will need to copy these three files into the DICOM directory:
 - Materials.dat
 - CMakeList.txt (A modified version from the original)
 - create_input.py

To create the input the code will look for the path that you are giving, by default DICOM/shared, and for the materials on the material list Materials.dat

Is important to run this code inside the build/ directory!

To run an example you can do:
'''
$ python create_input.py
'''

This will create automatically the Data.dat and copy the dicom images on the shared/ direcotry to the build directory.

To specify a path to the dicom images use the option "-p PATH_TO_DICOMS"
To choose a different name, use "-n OTHER_NAME"

If you have any questions, just e-mail me: fis.ebetancourt@gmail.com
